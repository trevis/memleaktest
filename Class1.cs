﻿using Decal.Adapter;
using Decal.Interop.Dat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MemLeakTest {
    [FriendlyName("MemLeakTest")]
    public class MemLeakTest : PluginBase {
        private int frameCounter = 0;
        private byte[] _buffer = new byte[10000];

        protected override void Startup() {
            Core.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            Core.RenderFrame += Core_RenderFrame;
        }

        
        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
                if (frameCounter++ < 10)
                    return;
                frameCounter = 0;
                Load(0x002B0000);
            }
            catch (Exception ex) {
                Core.Actions.AddChatText(ex.Message, 5);
            }
        }

        protected override void Shutdown() {
            Core.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
            Core.RenderFrame -= Core_RenderFrame;
        }

        private void Load(int landblock) {
            // land block info
            if (!GetCellFile(landblock + 0xFFFE, ref _buffer))
                return;

            int cellCount = BitConverter.ToInt32(_buffer, 4);
            int goodCells = 0;
            for (int index = 0; index <= cellCount; ++index) {
                //envcell
                if (!GetCellFile(landblock + 0x0100 + index, ref _buffer))
                    continue;
                goodCells++;
            }
            Core.Actions.AddChatText($"Loaded {goodCells}/{cellCount} cells", 5);
        }

        private static bool GetCellFile(int fileId, ref byte[] buffer) {
            return GetFile("services\\DecalDat.DatService\\cell\\" + fileId.ToString("X8"), ref buffer);
        }

        private static bool GetFile(string decalObject, ref byte[] buffer) {
            Array.Clear(buffer, 0, buffer.Length);
            IDatStream datStream = (IDatStream)CoreManager.Current.Decal.GetObject(decalObject, "{B27D3F72-2640-432F-BAE4-175E1AA0CA39}");
            if (datStream == null)
                return false;
            datStream.Restart();
            datStream.ReadBinary(datStream.Size, ref buffer[0]);
            Marshal.ReleaseComObject(datStream);
            return true;
        }
    }
}
